		/**
	 *在phpcms/modules/content/classes/content_tag.class.php中最后一个 } 上面增加函数
	 * 指定多ID列表内容调用标签
	 * {pc:content  action="listsid" catid="1,2,3" order="id DESC" num="10"}
	 * @param $data
	 * www.liloufan.com  BY:梦梵FLY  转载请保留
	 */
	public function listsid($data) {
		$arr_catid = explode(',',$data['catid']);
		if(isset($data['where'])) {
			$sql = $data['where'];
			} else {
				$thumb = intval($data['thumb']) ? " AND thumb != ''" : '';
				if(count($arr_catid) > 1){
					$val_arr = array();
					foreach($arr_catid as $key => $val)
					$str_catid = intval($val);
					if(!$this->set_modelid($str_catid)) return false;
						if($this->category[$val]['child']) {
							$catids_str = $this->category[$val]['arrchildid'];
							$pos = strpos($catids_str,',')+1;
							$val_arr[] = substr($catids_str, $pos);
						} else {
							$val_arr[] = $val;
						}
				}
				$imcatid = implode (",",$val_arr);
				$sql = "status=99 AND catid IN ($imcatid)".$thumb;
			}else{
				$catid = $arr_catid[0];
				if($this->category[$catid]['child']) {
					$catids_str = $this->category[$catid]['arrchildid'];
					$pos = strpos($catids_str,',')+1;
					$catids_str = substr($catids_str, $pos);
					$sql = "status=99 AND catid IN ($catids_str)".$thumb;
				} else {
					$sql = "status=99 AND catid='$catid'".$thumb;
				}
		}
	}
		$order = $data['order'];
		$return = $this->db->select($sql, '*', $data['limit'], $order, '', 'id');	
		//调用副表的数据
		if (isset($data['moreinfo']) && intval($data['moreinfo']) == 1) {
			$ids = array();
			foreach ($return as $v) {
				if (isset($v['id']) && !empty($v['id'])) {
					$ids[] = $v['id'];
				} else {
					continue;
				}
			}
			if (!empty($ids)) {
				$this->db->table_name = $this->db->table_name.'_data';
				$ids = implode('\',\'', $ids);
				$r = $this->db->select("`id` IN ('$ids')", '*', '', '', '', 'id');
				if (!empty($r)) {
					foreach ($r as $k=>$v) {
						if (isset($return[$k])) $return[$k] = array_merge($v, $return[$k]);
					}
				}
			}
		}
		return $return;
	}